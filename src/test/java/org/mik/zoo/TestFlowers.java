package org.mik.zoo;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.livingbeing.plant.flower.AbstractFlower;
import org.mik.zoo.livingbeing.plant.flower.Flower;
/**
 * @author Tölgyesi Dániel WX5HV8
 */
public class TestFlowers {

	private static final String COLOR = "color";
	
	private static final Integer ID = 1;
	private static final String SCIENTIFICNAME = "scientificName";
	private static final String INSTANCENAME = "instanceName";
	private static final String IMAGEURL = "imageURL";
	private static final int HEIGHT = 1;

	@Test
	public void classTest() throws NoSuchMethodException, SecurityException {
		/**
		 * Testing classes
		 */
		assertTrue(Flower.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractFlower.class.getModifiers()));
		
		/**
		 * Testing setter
		 */
		assertNotNull(AbstractFlower.class.getMethod("setHeight", int.class));
		assertNotNull(AbstractFlower.class.getMethod("setColor", String.class));
		/**
		 * Testing getter
		 */
		Method h = AbstractFlower.class.getMethod("getHeight");
		assertNotNull(h);
		assertEquals(h.getReturnType(), int.class);
		
		Method c = AbstractFlower.class.getMethod("getColor");
		assertNotNull(c);
		assertEquals(c.getReturnType(), String.class);
		
	}
	
	@Test
	public void objectTest() {

		Flower f1 = new Flower() {

			@Override
			public String getInstanceName() {
				return INSTANCENAME;
			}
			
			@Override
			public String getImageURL() {
				return IMAGEURL;
			}
			
			@Override
			public Integer getId() {
				return ID;
			}

			@Override
			public String getScientificName() {
				return SCIENTIFICNAME;
			}

			@Override
			public void setId(Integer id) {
				id = ID;
			}

			@Override
			public int getHeight() {
				return HEIGHT;
			}

			@Override
			public String getColor() {
				return COLOR;
			}

		};
		
		assertTrue(f1.getHeight() == HEIGHT);
		assertTrue(f1.getColor() == COLOR);
	}
}
