package org.mik.zoo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.LivingBeing;

/**
 * @author Tölgyesi Dániel WX5HV8
 */

public class TestLivingBeings {
	
	private static final Integer ID = 1;
	private static final String SCIENTIFICNAME = "scientificName";
	private static final String INSTANCENAME = "instanceName";
	private static final String IMAGEURL = "imageURL";
	
	@Test
	public void classTest() throws NoSuchMethodException, SecurityException {
		/**
		 * Testing classes
		 */
		assertTrue(LivingBeing.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractLivingBeing.class.getModifiers()));
		
		/**
		 * Testing setters
		 */
		assertNotNull(AbstractLivingBeing.class.getMethod("setScientificName", String.class));
		assertNotNull(AbstractLivingBeing.class.getMethod("setInstanceName", String.class));
		assertNotNull(AbstractLivingBeing.class.getMethod("setImageURL", String.class));
		
		/**
		 * Testing getters
		 */
		Method sn = AbstractLivingBeing.class.getMethod("getScientificName");
		assertNotNull(sn);
		assertEquals(sn.getReturnType(), String.class);
		
		Method in = AbstractLivingBeing.class.getMethod("getInstanceName");
		assertNotNull(in);
		assertEquals(in.getReturnType(), String.class);
		
		Method url = AbstractLivingBeing.class.getMethod("getImageURL");
		assertNotNull(url);
		assertEquals(url.getReturnType(), String.class);
		
		/**
		 * Testing constructors
		 */
		Constructor<?> c = AbstractLivingBeing.class.getConstructor(String.class);
		assertNotNull(c);
	}
	
	@Test
	public void objectTest() {

		LivingBeing l1 = new LivingBeing() {

			@Override
			public String getInstanceName() {
				return INSTANCENAME;
			}
			
			@Override
			public String getImageURL() {
				return IMAGEURL;
			}
			
			@Override
			public Integer getId() {
				return ID;
			}

			@Override
			public String getScientificName() {
				return SCIENTIFICNAME;
			}

			@Override
			public void setId(Integer id) {
				id = ID;
			}
		};
		
		assertTrue(l1.getId().equals(ID));
		assertTrue(l1.getInstanceName().equals(INSTANCENAME));
		assertTrue(l1.getScientificName().equals(SCIENTIFICNAME));
		assertTrue(l1.getImageURL().equals(IMAGEURL));
		
		l1.setId(0);
		
	}
	
}











