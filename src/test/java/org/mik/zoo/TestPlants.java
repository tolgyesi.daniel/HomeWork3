package org.mik.zoo;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.livingbeing.plant.AbstractPlant;
import org.mik.zoo.livingbeing.plant.Plant;
/**
 * @author Tölgyesi Dániel WX5HV8
 */
public class TestPlants {
	
	
	private static final Integer ID = 1;
	private static final String SCIENTIFICNAME = "scientificName";
	private static final String INSTANCENAME = "instanceName";
	private static final String IMAGEURL = "imageURL";
	private static final int HEIGHT = 1;

	@Test
	public void classTest() throws NoSuchMethodException, SecurityException {
		/**
		 * Testing classes
		 */
		assertTrue(Plant.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractPlant.class.getModifiers()));
		
		/**
		 * Testing setter
		 */
		assertNotNull(AbstractPlant.class.getMethod("setHeight", int.class));
		/**
		 * Testing getter
		 */
		Method h = AbstractPlant.class.getMethod("getHeight");
		assertNotNull(h);
		assertEquals(h.getReturnType(), int.class);
		
	}
	
	@Test
	public void objectTest() {

		Plant p1 = new Plant() {

			@Override
			public String getInstanceName() {
				return INSTANCENAME;
			}
			
			@Override
			public String getImageURL() {
				return IMAGEURL;
			}
			
			@Override
			public Integer getId() {
				return ID;
			}

			@Override
			public String getScientificName() {
				return SCIENTIFICNAME;
			}

			@Override
			public void setId(Integer id) {
				id = ID;
			}

			@Override
			public int getHeight() {
				return HEIGHT;
			}

		};
		
		assertTrue(p1.getHeight() == HEIGHT);
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
