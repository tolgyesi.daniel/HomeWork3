package org.mik.zoo;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.livingbeing.plant.tree.AbstractTree;
import org.mik.zoo.livingbeing.plant.tree.Tree;
/**
 * @author Tölgyesi Dániel WX5HV8
 */
public class TestTrees {

	private static final boolean DECIDOUOS = false;
	
	private static final Integer ID = 1;
	private static final String SCIENTIFICNAME = "scientificName";
	private static final String INSTANCENAME = "instanceName";
	private static final String IMAGEURL = "imageURL";
	private static final int HEIGHT = 1;

	@Test
	public void classTest() throws NoSuchMethodException, SecurityException {
		/**
		 * Testing classes
		 */
		assertTrue(Tree.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractTree.class.getModifiers()));
		
		/**
		 * Testing setter
		 */
		assertNotNull(AbstractTree.class.getMethod("setHeight", int.class));
		assertNotNull(AbstractTree.class.getMethod("setDeciduous", boolean.class));
		/**
		 * Testing getter
		 */
		Method h = AbstractTree.class.getMethod("getHeight");
		assertNotNull(h);
		assertEquals(h.getReturnType(), int.class);
		
		Method d = AbstractTree.class.getMethod("getDeciduous");
		assertNotNull(d);
		assertEquals(d.getReturnType(), boolean.class);
		
	}
	
	@Test
	public void objectTest() {

		Tree t1 = new Tree() {

			@Override
			public String getInstanceName() {
				return INSTANCENAME;
			}
			
			@Override
			public String getImageURL() {
				return IMAGEURL;
			}
			
			@Override
			public Integer getId() {
				return ID;
			}

			@Override
			public String getScientificName() {
				return SCIENTIFICNAME;
			}

			@Override
			public void setId(Integer id) {
				id = ID;
			}

			@Override
			public int getHeight() {
				return HEIGHT;
			}

			@Override
			public boolean getDeciduous() {
				return DECIDOUOS;
			}

		};
		
		assertTrue(t1.getHeight() == HEIGHT);
		assertTrue(t1.getDeciduous() == DECIDOUOS);
	}

}
