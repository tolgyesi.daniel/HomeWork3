package org.mik.zoo;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.livingbeing.animal.AnimalType;
import org.mik.zoo.livingbeing.animal.mammal.AbstractMammal;
import org.mik.zoo.livingbeing.animal.mammal.Mammal;
/**
 * @author Tölgyesi Dániel WX5HV8
 */
public class TestMammals {

	private static final int LENGTHOFHAIR = 1;
	
	private static final Integer ID = 1;
	private static final String SCIENTIFICNAME = "scientificName";
	private static final String INSTANCENAME = "instanceName";
	private static final String IMAGEURL = "imageURL";
	
	private static final AnimalType ANIMALTYPE = null;
	private static final int NUMBEROFLEGS = 1;
	private static final int NUMBEROFTEETH = 1;
	private static final int WEIGHT = 1;
	
	@Test
	public void classTest() throws NoSuchMethodException, SecurityException {
		
		/**
		 * Testing classes
		 */
		assertTrue(Mammal.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractMammal.class.getModifiers()));
		
		/**
		 * Testing setters
		 */
		assertNotNull(AbstractMammal.class.getMethod("setLengthOfHair", int.class));
		/**
		 * Testing getters
		 */
		Method lh = AbstractMammal.class.getMethod("getLengthOfHair");
		assertNotNull(lh);
		assertEquals(lh.getReturnType(), int.class);
	}
	@Test
	public void objectTest() {
		
		Mammal m1 = new Mammal() {

			@Override
			public String getInstanceName() {
				return INSTANCENAME;
			}
			
			@Override
			public String getImageURL() {
				return IMAGEURL;
			}
			
			@Override
			public Integer getId() {
				return ID;
			}

			@Override
			public String getScientificName() {
				return SCIENTIFICNAME;
			}

			@Override
			public void setId(Integer id) {
				id = ID;
			}

			@Override
			public AnimalType getAnimalType() {
				return ANIMALTYPE;
			}

			@Override
			public int getNumberOfLegs() {
				return NUMBEROFLEGS;
			}

			@Override
			public int getNumberOfTeeth() {
				return NUMBEROFTEETH;
			}

			@Override
			public int getWeight() {
				return WEIGHT;
			}

			@Override
			public int getLengthOfHair() {
				return LENGTHOFHAIR;
			}
		};
		
		assertTrue(m1.getLengthOfHair() == LENGTHOFHAIR);
		
	}

}













