package org.mik.zoo;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.Animal;
import org.mik.zoo.livingbeing.animal.AnimalType;

/**
 * @author Tölgyesi Dániel WX5HV8
 */

public class TestAnimals {
	
	private static final AnimalType ANIMALTYPE = null;
	private static final int NUMBEROFLEGS = 1;
	private static final int NUMBEROFTEETH = 1;
	private static final int WEIGHT = 1;
	
	private static final Integer ID = 1;
	private static final String SCIENTIFICNAME = "scientificName";
	private static final String INSTANCENAME = "instanceName";
	private static final String IMAGEURL = "imageURL";
	
	@Test
	public void classTest() throws NoSuchMethodException, SecurityException {
		/**
		 * Testing classes
		 */
		assertTrue(Animal.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractAnimal.class.getModifiers()));
		
		/**
		 * Testing setters
		 */
		assertNotNull(AbstractAnimal.class.getMethod("setNumberOfLegs", int.class));
		assertNotNull(AbstractAnimal.class.getMethod("setNumberOfTeeth", int.class));
		assertNotNull(AbstractAnimal.class.getMethod("setWeight", int.class));
		
		/**
		 * Testing getters
		 */
		Method type = AbstractAnimal.class.getMethod("getAnimalType");
		assertNotNull(type);
		assertEquals(type.getReturnType(), AnimalType.class);
		
		Method nl = AbstractAnimal.class.getMethod("getNumberOfLegs");
		assertNotNull(nl);
		assertEquals(nl.getReturnType(), int.class);
		
		Method nt = AbstractAnimal.class.getMethod("getNumberOfTeeth");
		assertNotNull(nt);
		assertEquals(nt.getReturnType(), int.class);
		
		Method w = AbstractAnimal.class.getMethod("getWeight");
		assertNotNull(w);
		assertEquals(w.getReturnType(), int.class);
		
	}
	
	@Test
	public void objectTest() {

		Animal a1 = new Animal() {

			@Override
			public String getInstanceName() {
				return INSTANCENAME;
			}
			
			@Override
			public String getImageURL() {
				return IMAGEURL;
			}
			
			@Override
			public Integer getId() {
				return ID;
			}

			@Override
			public String getScientificName() {
				return SCIENTIFICNAME;
			}

			@Override
			public void setId(Integer id) {
				id = ID;
			}

			@Override
			public AnimalType getAnimalType() {
				return ANIMALTYPE;
			}

			@Override
			public int getNumberOfLegs() {
				return NUMBEROFLEGS;
			}

			@Override
			public int getNumberOfTeeth() {
				return NUMBEROFTEETH;
			}

			@Override
			public int getWeight() {
				return WEIGHT;
			}
		};
		
		assertTrue(a1.getNumberOfLegs() == NUMBEROFLEGS);
		assertTrue(a1.getNumberOfTeeth() == NUMBEROFTEETH);
		assertTrue(a1.getWeight() == WEIGHT);
	
	}
	
}







