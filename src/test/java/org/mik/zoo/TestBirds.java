package org.mik.zoo;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.livingbeing.animal.AnimalType;
import org.mik.zoo.livingbeing.animal.bird.AbstractBird;
import org.mik.zoo.livingbeing.animal.bird.Bird;
/**
 * @author Tölgyesi Dániel WX5HV8
 */
public class TestBirds {

	private static final int WINGLENGTH = 1;
	
	private static final Integer ID = 1;
	private static final String SCIENTIFICNAME = "scientificName";
	private static final String INSTANCENAME = "instanceName";
	private static final String IMAGEURL = "imageURL";
	
	private static final AnimalType ANIMALTYPE = null;
	private static final int NUMBEROFLEGS = 1;
	private static final int NUMBEROFTEETH = 1;
	private static final int WEIGHT = 1;
	
	@Test
	public void classTest() throws NoSuchMethodException, SecurityException {
		
		/**
		 * Testing classes
		 */
		assertTrue(Bird.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractBird.class.getModifiers()));
		
		/**
		 * Testing setters
		 */
		assertNotNull(AbstractBird.class.getMethod("setWingLength", int.class));
		/**
		 * Testing getters
		 */
		Method wl = AbstractBird.class.getMethod("getWingLength");
		assertNotNull(wl);
		assertEquals(wl.getReturnType(), int.class);
	}
	@Test
	public void objectTest() {
		
		Bird b1 = new Bird() {

			@Override
			public String getInstanceName() {
				return INSTANCENAME;
			}
			
			@Override
			public String getImageURL() {
				return IMAGEURL;
			}
			
			@Override
			public Integer getId() {
				return ID;
			}

			@Override
			public String getScientificName() {
				return SCIENTIFICNAME;
			}

			@Override
			public void setId(Integer id) {
				id = ID;
			}

			@Override
			public AnimalType getAnimalType() {
				return ANIMALTYPE;
			}

			@Override
			public int getNumberOfLegs() {
				return NUMBEROFLEGS;
			}

			@Override
			public int getNumberOfTeeth() {
				return NUMBEROFTEETH;
			}

			@Override
			public int getWeight() {
				return WEIGHT;
			}

			@Override
			public int getWingLength() {
				return WINGLENGTH;
			}
		};
		
		assertTrue(b1.getWingLength() == WINGLENGTH);
		
	}

}
