package org.mik.zoo;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.livingbeing.animal.AnimalType;
import org.mik.zoo.livingbeing.animal.fish.AbstractFish;
import org.mik.zoo.livingbeing.animal.fish.Fish;
/**
 * @author Tölgyesi Dániel WX5HV8
 */
public class TestFishes {

	private static final int NUMBEROFFINS = 1;
	
	private static final Integer ID = 1;
	private static final String SCIENTIFICNAME = "scientificName";
	private static final String INSTANCENAME = "instanceName";
	private static final String IMAGEURL = "imageURL";
	
	private static final AnimalType ANIMALTYPE = null;
	private static final int NUMBEROFLEGS = 1;
	private static final int NUMBEROFTEETH = 1;
	private static final int WEIGHT = 1;
	
	@Test
	public void classTest() throws NoSuchMethodException, SecurityException {
		
		/**
		 * Testing classes
		 */
		assertTrue(Fish.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractFish.class.getModifiers()));
		
		/**
		 * Testing setters
		 */
		assertNotNull(AbstractFish.class.getMethod("setnumberOfFins", int.class));
		/**
		 * Testing getters
		 */
		Method nf = AbstractFish.class.getMethod("getNumberOfFins");
		assertNotNull(nf);
		assertEquals(nf.getReturnType(), int.class);
	}
	@Test
	public void objectTest() {
		
		Fish f1 = new Fish() {

			@Override
			public String getInstanceName() {
				return INSTANCENAME;
			}
			
			@Override
			public String getImageURL() {
				return IMAGEURL;
			}
			
			@Override
			public Integer getId() {
				return ID;
			}

			@Override
			public String getScientificName() {
				return SCIENTIFICNAME;
			}

			@Override
			public void setId(Integer id) {
				id = ID;
			}

			@Override
			public AnimalType getAnimalType() {
				return ANIMALTYPE;
			}

			@Override
			public int getNumberOfLegs() {
				return NUMBEROFLEGS;
			}

			@Override
			public int getNumberOfTeeth() {
				return NUMBEROFTEETH;
			}

			@Override
			public int getWeight() {
				return WEIGHT;
			}

			@Override
			public int getNumberOfFins() {
				return NUMBEROFFINS;
			}
		};
		
		assertTrue(f1.getNumberOfFins() == NUMBEROFFINS);
		
	}

}
