/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.plant.tree;

public class GiantRedwood extends AbstractTree {

	public static final String SCIENTIFIC_NAME = "Sequoiadendron giganteum"; //$NON-NLS-1$

	public GiantRedwood() {
		super(SCIENTIFIC_NAME, 0);
	}

	public GiantRedwood(String instanceName, String imageURL, int height) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, height);
	}

	public GiantRedwood(String instanceName, String imageURL, int height, boolean deciduous
			) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, height, deciduous);
	}
}