/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.plant;

import org.mik.zoo.livingbeing.AbstractLivingBeing;

public abstract class AbstractPlant extends AbstractLivingBeing implements Plant {

	private int height;
	
	public AbstractPlant(String scientificName) {
		this(scientificName, 0);
	}

	public AbstractPlant(String scientificName, int height) {
		this(scientificName, null, null, height);
	}

	public AbstractPlant(String scientificName, String instanceNamme, String imageURL, int height) {
		super(scientificName, instanceNamme, imageURL);
		this.height = height;
	}

	
	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	

}
