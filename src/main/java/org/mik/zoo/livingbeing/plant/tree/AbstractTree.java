/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.plant.tree;

import org.mik.zoo.livingbeing.plant.AbstractPlant;

public abstract class AbstractTree extends AbstractPlant implements Tree {

	private boolean deciduous;
	
	public AbstractTree(String scientificName, int height) {
		this(scientificName, null, null, height);
	}

	public AbstractTree(String scientificName, String instanceName, String imageURL, int height) {
		this(scientificName, instanceName, imageURL, height, false);
	}

	public AbstractTree(String scientificName, String instanceNamme, String imageURL, int height, boolean deciduous) {
		super(scientificName, instanceNamme, imageURL, height);
		this.deciduous = deciduous;
	}

	public boolean getDeciduous() {
		return this.deciduous;
	}

	public void setDeciduous(boolean deciduous) {
		this.deciduous = deciduous;
	}

}
