/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.plant.flower;

public class FlyTrap extends AbstractFlower {

	public static final String SCIENTIFIC_NAME = "Dionaea muscipula"; //$NON-NLS-1$

	public FlyTrap() {
		super(SCIENTIFIC_NAME, 0);
	}

	public FlyTrap(String instanceName, String imageURL, int height) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, height);
	}

	public FlyTrap(String instanceName, String imageURL, int height, String color
			) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, height, color);
	}
}
