/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.plant.flower;

import org.mik.zoo.livingbeing.plant.AbstractPlant;

public abstract class AbstractFlower extends AbstractPlant implements Flower {

	public String color;
	
	public AbstractFlower(String scientificName, int height) {
		this(scientificName, null, null, height);
	}

	public AbstractFlower(String scientificName, String instanceName, String imageURL, int height) {
		this(scientificName, instanceName, imageURL, height, null);
	}

	public AbstractFlower(String scientificName, String instanceNamme, String imageURL, int height, String color) {
		super(scientificName, instanceNamme, imageURL, height);
		this.color = color;
	}

	
	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
}
