/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.plant.tree;

import org.mik.zoo.livingbeing.plant.Plant;

public interface Tree extends Plant{

	public boolean getDeciduous();
	
}
