/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.plant.flower;

import org.mik.zoo.livingbeing.plant.Plant;

public interface Flower extends Plant{

	public String getColor();
	
}
