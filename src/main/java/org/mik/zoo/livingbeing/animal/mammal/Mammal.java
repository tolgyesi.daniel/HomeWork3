/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal.mammal;

import org.mik.zoo.livingbeing.animal.Animal;

public interface Mammal extends Animal {

	public int getLengthOfHair();
}