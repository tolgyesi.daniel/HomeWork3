/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal.bird;

import org.mik.zoo.livingbeing.animal.Animal;

public interface Bird extends Animal {

	public int getWingLength();
}