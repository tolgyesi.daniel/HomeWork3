/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal.mammal;

import org.mik.zoo.livingbeing.animal.AnimalType;

public class Platypus extends AbstractMammal {

	public static final String SCIENTIFIC_NAME = "Ornithorhynchus anatinus"; //$NON-NLS-1$
	public static final AnimalType TYPE = AnimalType.HERBIVOROUS;

	public Platypus() {
		super(SCIENTIFIC_NAME, TYPE);
	}

	public Platypus(String instanceName, String imageURL) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE);
	}

	public Platypus(String instanceName, String imageURL, int numberOfLegs, int numberOfTeeth, int weight,
			int lengthOfHair) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE, numberOfLegs, numberOfTeeth, weight, lengthOfHair);
	}
    @Override
    public String toString() {
    	return SCIENTIFIC_NAME + ": Platypus";
    }
}
