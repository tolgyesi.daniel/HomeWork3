/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal.bird;

import org.mik.zoo.livingbeing.animal.AnimalType;

public class Ostrich extends AbstractBird {

	public static final String SCIENTIFIC_NAME = "Struthio camelus"; //$NON-NLS-1$
	public static final AnimalType TYPE = AnimalType.PREDATOR;

	public Ostrich() {
		super(SCIENTIFIC_NAME, TYPE);
	}

	public Ostrich(String instanceName, String imageURL) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE);
	}

	public Ostrich(String instanceName, String imageURL, int numberOfLegs, int numberOfTeeth, int weight,
			int wingLength) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE, numberOfLegs, numberOfTeeth, weight, wingLength);
	}
    @Override
    public String toString() {
       return SCIENTIFIC_NAME + ": Ostrich";
    }
}