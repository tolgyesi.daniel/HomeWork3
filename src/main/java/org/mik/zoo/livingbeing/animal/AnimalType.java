/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal;

public enum AnimalType {
	PREDATOR, HERBIVOROUS
}
