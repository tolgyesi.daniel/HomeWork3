/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal.fish;

import org.mik.zoo.livingbeing.animal.AnimalType;

public class GreatWhiteShark extends AbstractFish {

	public static final String SCIENTIFIC_NAME = "Carcharodon carcharias"; //$NON-NLS-1$
	public static final AnimalType TYPE = AnimalType.PREDATOR;

	public GreatWhiteShark() {
		super(SCIENTIFIC_NAME, TYPE);
	}

	public GreatWhiteShark(String instanceName, String imageURL) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE);
	}

	public GreatWhiteShark(String instanceName, String imageURL, int numberOfLegs, int numberOfTeeth, int weight,
			int numberOfFins) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE, numberOfLegs, numberOfTeeth, weight, numberOfFins);
	}
    @Override
    public String toString() {
    	return SCIENTIFIC_NAME + ": Great White Shark";
    }
}