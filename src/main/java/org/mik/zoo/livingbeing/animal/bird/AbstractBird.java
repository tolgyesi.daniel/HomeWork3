/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal.bird;

import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.AnimalType;

public abstract class AbstractBird extends AbstractAnimal implements Bird {
	
	private int wingLength;

	public AbstractBird(String scientificName, AnimalType animalType) {
		this(scientificName, null, null, animalType);
	}

	public AbstractBird(String scientificName, String instanceName, String imageURL, AnimalType animalType) {
		this(scientificName, instanceName, imageURL, animalType, 0, 0, 0, 0);
	}
	
	public AbstractBird(String scientificName, String instanceName, String imageURL, AnimalType animalType,
			int numberOfLegs, int numberOfTeeth, int weight, int wingLength) {
		super(scientificName, instanceName, imageURL, animalType, numberOfLegs, numberOfTeeth, weight);
		this.wingLength = wingLength;
	}

	@Override
	public int getWingLength() {
		return this.wingLength;
	}
	
	public void setWingLength(int wingLength) {
		this.wingLength = wingLength;
	}

}
