/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal.bird;

import org.mik.zoo.livingbeing.animal.AnimalType;

public class Penguin extends AbstractBird {

	public static final String SCIENTIFIC_NAME = "Aptenodytes forsteri"; //$NON-NLS-1$
	public static final AnimalType TYPE = AnimalType.PREDATOR;

	public Penguin() {
		super(SCIENTIFIC_NAME, TYPE);
	}

	public Penguin(String instanceName, String imageURL) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE);
	}

	public Penguin(String instanceName, String imageURL, int numberOfLegs, int numberOfTeeth, int weight,
			int wingLength) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE, numberOfLegs, numberOfTeeth, weight, wingLength);
	}
    
	@Override
    public String toString() {
       return SCIENTIFIC_NAME + ": Penguin";
    }
}
