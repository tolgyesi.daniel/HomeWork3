/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing.animal.fish;

import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.AnimalType;

public abstract class AbstractFish extends AbstractAnimal implements Fish {
	
	private int numberOfFins;

	public AbstractFish(String scientificName, AnimalType animalType) {
		this(scientificName, null, null, animalType);
	}

	public AbstractFish(String scientificName, String instanceName, String imageURL, AnimalType animalType) {
		this(scientificName, instanceName, imageURL, animalType, 0, 0, 0, 0);
	}
	
	public AbstractFish(String scientificName, String instanceName, String imageURL, AnimalType animalType,
			int numberOfLegs, int numberOfTeeth, int weight, int numberOfFins) {
		super(scientificName, instanceName, imageURL, animalType, numberOfLegs, numberOfTeeth, weight);
		this.numberOfFins = numberOfFins;
	}

	@Override
	public int getNumberOfFins() {
		return this.numberOfFins;
	}
	
	public void setnumberOfFins(int numberOfFins) {
		this.numberOfFins = numberOfFins;
	}

}
