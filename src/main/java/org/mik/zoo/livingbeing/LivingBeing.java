/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.livingbeing;

public interface LivingBeing {

	public String getScientificName();

	public String getInstanceName();

	public String getImageURL();

	public Integer getId();
	
	public void setId(Integer id);
}
