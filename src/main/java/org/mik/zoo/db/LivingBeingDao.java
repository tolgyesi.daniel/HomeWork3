/**
 * @author Tölgyesi Dániel WX5HV8
 */
package org.mik.zoo.db;

import java.util.List;

import org.mik.zoo.livingbeing.LivingBeing;

public interface LivingBeingDao {
	
	public LivingBeing findOne(Integer id);
	
	public List<LivingBeing> findAll();
	
	public List<LivingBeing> findByScientificName(String name);
	
	public List<LivingBeing> findByInstanceName(String name);
	
	public LivingBeing persist(LivingBeing livingBeing);
	
	public boolean delete(LivingBeing livingBeing);

	public void initDb();
	
}
