package org.mik.zoo;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.db.LivingBeingDao;
import org.mik.zoo.db.LivingBeingDaoImpl;
import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.Animal;
import org.mik.zoo.livingbeing.animal.AnimalType;
import org.mik.zoo.livingbeing.animal.bird.Bird;
import org.mik.zoo.livingbeing.animal.bird.Ostrich;
import org.mik.zoo.livingbeing.animal.bird.Penguin;
import org.mik.zoo.livingbeing.animal.fish.Fish;
import org.mik.zoo.livingbeing.animal.fish.GreatWhiteShark;
import org.mik.zoo.livingbeing.animal.mammal.Elephant;
import org.mik.zoo.livingbeing.animal.mammal.Rhinoceros;
import org.mik.zoo.livingbeing.plant.flower.Flower;
import org.mik.zoo.livingbeing.plant.flower.FlyTrap;
import org.mik.zoo.livingbeing.plant.tree.Tree;

/**
 * Main class
 *
 * @author Tölgyesi Dániel WX5HV8
 *
 */
@SuppressWarnings("unused")
public class App {

	/**
	 * Name of the data file
	 */
	private static final Logger LOG = LogManager.getLogger(App.class);
	
	private static final String DB_INIT_FILE_NAME = "/init.sql";
	private LivingBeingDao livingBeingDao = new LivingBeingDaoImpl(DB_INIT_FILE_NAME);
	/**
	 * All living being
	 */
	private List<LivingBeing> livingBeings;
	/**
	 * 
	 */
	private Map<AnimalType, List<Animal>> animalsByType;

	/**
	 * Constructor for the application
	 */
	public App() {
		this.livingBeings = new ArrayList<>();
		this.animalsByType = new HashMap<>();
	}

	/**
	 * Entry point of the application
	 *
	 * @param args
	 *            arguments
	 */
	public static void main(String[] args) {
		new App().start();
	}

	/**
	 * Invoke methods
	 */
	private void start() {
		this.loadData();
		this.livingBeingDao.initDb();
		this.organizeAnimals();
		this.printAnswers();
		this.printAnimalsByType();
	}

	/**
	 * Load data
	 */
	private void loadData() {
		this.livingBeings.addAll(livingBeingDao.findAll());
	}

	/**
	 * Answer the questions of the kids visiting the zoo
	 */
	@SuppressWarnings("boxing")
	private void printAnswers() {
		System.out.println("\nQuestions - answers\n"); //$NON-NLS-1$
		
		System.out.println("How many animals are there here?"); //$NON-NLS-1$
		System.out.println(String.format("animal count: %d\n", this.countAnimals())); //$NON-NLS-1$
		
		System.out.println("How many elephants are there here?"); //$NON-NLS-1$
		System.out.println(String.format("elephant count: %d\n", this.countElephants())); //$NON-NLS-1$
		
		System.out.println("Dumbo is here?");//$NON-NLS-1$ 
		System.out.println(String.format("We have Dumbo? %s\n", this.searchDumbo())); //$NON-NLS-1$
		
		System.out.println("How many predators are there?"); //$NON-NLS-1$
		System.out.println(String.format("predators count: %d\n", this.countPredators())); //$NON-NLS-1$
		
		System.out.println("List all Predators:"); this.listPredators(); //$NON-NLS-1$
		System.out.print("\n");
		
		System.out.println("List all Elephant:"); this.listElephant(); //$NON-NLS-1$
		System.out.print("\n");
		
		System.out.println("How many trees are there?"); //$NON-NLS-1$
		System.out.println(String.format("trees count: %d\n", this.countTrees())); //$NON-NLS-1$
		
		System.out.println("How many flowers are there?"); //$NON-NLS-1$
		System.out.println(String.format("flowers count: %d\n", this.countFlowers())); //$NON-NLS-1$
		
		System.out.println("Is there a whiteshark, and what is the name of it?"); this.searchShark(); //$NON-NLS-1$
		System.out.println("\n");
		
		System.out.println("How many rhinos are there?"); //$NON-NLS-1$
		System.out.println(String.format("Rhinos count: %d\n", this.countRhinos())); //$NON-NLS-1$
		
		System.out.println("Is there a giant sequioa there?");
		System.out.println(String.format("We have giant sequioa? %s\n", this.searchGiant()));//$NON-NLS-1$
		
		System.out.println("Is there a Venus flytrap there?"); 
		System.out.println(String.format("We have Venus flytrap? %s\n", this.searchVenus()));//$NON-NLS-1$

		System.out.println("Is Tux here?"); //$NON-NLS-1$
		System.out.println(String.format("We have Tux? %s\n", this.searchTux())); //$NON-NLS-1$
		
		System.out.println("How many birds are there here?"); //$NON-NLS-1$
		System.out.println(String.format("bird count: %d\n", this.countBird())); //$NON-NLS-1$
		
		System.out.println("How many ostrich are there here?"); //$NON-NLS-1$
		System.out.println(String.format("ostrich count: %d\n", this.countOstrich())); //$NON-NLS-1$
		
		System.out.println("Is there a barracuda there?"); //$NON-NLS-1$
		System.out.println(String.format("We have Barracuda? %s\n", this.searchBarracuda())); //$NON-NLS-1$
		
		System.out.println("How many fishes are there here?"); //$NON-NLS-1$
		System.out.println(String.format("fish count: %d\n", this.countFish())); //$NON-NLS-1$
	}

	/**
	 * Count animals
	 *
	 * @return number of animals
	 */
	private int countAnimals() {
		int animalCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Animal) {
				animalCount++;
			}
		}
		
		return animalCount;
	}

	/**
	 * Count elephants
	 *
	 * @return number of elephants
	 */
	private int countElephants() {
		int elephantCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Elephant) {
				elephantCount++;
			}
		}
		
		return elephantCount;
	}
	
	/**
	 * Search Dumbo
	 * 
	 * @return yes or no
	 */
	
	private String searchDumbo() {
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Elephant) {
				if(livingBeing.getInstanceName().equals("Dumbo")) {
					return "yes";
				}
				else {
					return "no";
				}
			}
		}
		return null;
	}
	
	/**
	 * Count Predators
	 * 
	 * return number of predators
	 */
	
	private int countPredators() {
		int predatorsCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Animal) {
				if(((Animal) livingBeing).getAnimalType() == AnimalType.PREDATOR) {
					predatorsCount++;
				}
			}
		}
		
		return predatorsCount;
	}
	
	/**
	 * write out all predators
	 */
	
	private void listPredators() {
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Animal) {
				if(((Animal) livingBeing).getAnimalType() == AnimalType.PREDATOR) {
					System.out.println(printAnimals(livingBeing.getScientificName())+", "+livingBeing.getScientificName());
				}
			}
		}
		
	}
	
	/**
	 * 
	 * @param scientificName
	 * 		Create unique name all of animals
	 * @return unique name
	 */
	
	private String printAnimals(String scientificName) {
		String elephant = "Loxodonta africana";
		String lion = "Panthera leo";
		String Platypus = "Ornithorhynchus anatinus";
		String Shark = "Carcharodon carcharias";
		String Ostrich = "Struthio camelus";
		String Rhinoceros = "Ceratotherium simum simum";
		String Penguin = "Aptenodytes forsteri";
		
		if(scientificName == elephant) {
			return "Elephant";
		}
		else if(scientificName == lion) {
			return "Lion";
		}
		else if(scientificName == Platypus) {
			return "Platypus";
		}
		else if(scientificName == Shark) {
			return "Great white shark";
		}
		else if(scientificName == Ostrich) {
			return "Ostrich";
		}
		else if(scientificName == Rhinoceros) {
			return "Rhinoceros";
		}
		else if(scientificName == Penguin) {
			return "Penguin";
		}
		
		return null;
	}
	
	/**
	 * list all Elephant
	 */

	private void listElephant() {
		String name;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Elephant) {
				if(livingBeing.getInstanceName() == null) {
					name = "Don't have name";
				}
				else {
					name = livingBeing.getInstanceName();
				}
				System.out.println(printAnimals(livingBeing.getScientificName())+", "+ livingBeing.getScientificName()+", "+name);
			}
		}
		
	}
	/**
	 * Count trees
	 * @return treesCount
	 */
	private int countTrees() {
		int treesCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Tree) {
				treesCount++;
			}
		}
		
		return treesCount;
	}
	/**
	 * Count flowers
	 * @return flowersCount
	 */
	private int countFlowers() {
		int flowersCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Flower) {
				flowersCount++;
			}
		}
		
		return flowersCount;
	}
	/**
	 * Search shark and print out name
	 */
	private void searchShark() {
		boolean here = false;
		
		System.out.print("We have White shark? ");
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof GreatWhiteShark) {
				here = true;
			}
			else {
				here = false;
			}
			
			if(here) {
				System.out.printf("yes, name: %s", livingBeing.getInstanceName());
			}
			else {
				System.out.println("we don't have");
			}
		}
	}
	/**
	 * count all rhinos
	 * @return rhinosCount
	 */
	private int countRhinos() {
		int rhinosCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Rhinoceros) {
				rhinosCount++;
			}
		}
		
		return rhinosCount;
	}
	/**
	 * Search giant sequioa
	 * @return yes or no
	 */
	private String searchGiant() {
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Tree) {
				if(livingBeing.getScientificName().equals("giant sequioa")) {
					return "yes";
				}
				else {
					return "no";
				}
			}
		}
		return null;
	}
	
	private String searchVenus() {
		
		boolean here = false;
		String answer = null;
		
		System.out.print("We have Venus flytrap? ");
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof FlyTrap) {
				here = true;
			}
			else {
				here = false;
			}
			
			if(here) {
				answer = "yes";
			}
			else {
				answer = "no";
			}
		}
		return answer;
	}
	/**
	 * Search Tux
	 * @return yes or no
	 */
	private String searchTux() {
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Penguin) {
				if(livingBeing.getInstanceName().equals("Tux")) {
					return "yes";
				}
				else {
					return "no";
				}
			}
		}
		return null;
	}
	
	/**
	 * Count birds
	 *
	 * @return number of birds
	 */
	private int countBird() {
		int birdCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Bird) {
				birdCount++;
			}
		}
		
		return birdCount;
	}
	/**
	 * Count ostrich
	 *
	 * @return number of ostrich
	 */
	private int countOstrich() {
		int ostrichCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Ostrich) {
				ostrichCount++;
			}
		}
		
		return ostrichCount;
	}
	/**
	 * Search barracuda
	 * @return yes or no
	 */
	private String searchBarracuda() {
		 try {
			 Class.forName("Barracuda");
			 return "yes";
		 } catch (ClassNotFoundException e) {
			 return "no";
		 }
	}
	/**
	 * Count fishes
	 *
	 * @return number of fishes
	 */
	private int countFish() {
		int fishCount = 0;
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing instanceof Fish) {
				fishCount++;
			}
		}
		
		return fishCount;
	}
	/**
	 * Find a living being by the given name
	 *
	 * @param name
	 *            the name
	 * @return LivingBeing instance if found or null, if the name was null or no
	 *         LivingBeing for this name
	 */
	private LivingBeing findByName(String name) {
		
		for(LivingBeing livingBeing : this.livingBeings) {
			if(livingBeing.getInstanceName().equals(name)) {
				System.out.println(livingBeing.getScientificName()+", "+livingBeing.getInstanceName()+", "
						+livingBeing.getImageURL());
			}
			else {
				System.out.println("We don't have animal like this name!");
			}
		}
		return null;
	}

	/**
	 * Collect animals by their types
	 */
	private void organizeAnimals() {
		for (LivingBeing livingBeing : this.livingBeings) {
			if (livingBeing instanceof Animal) {
				Animal animal = (Animal)livingBeing;
				AnimalType animalType = animal.getAnimalType();
				
				List<Animal> list = this.animalsByType.get(animalType);
				
				if(list == null) {
					list = new ArrayList<>();
				}
				
				list.add(animal);
				
				this.animalsByType.put(animalType, list);
			}
		}
	}

	/**
	 * Printing animals by their types
	 */
	@SuppressWarnings("rawtypes")
	private void printAnimalsByType() {
		System.out.println("\nAnimals by type\n"); //$NON-NLS-1$
		
		for (Map.Entry entry : animalsByType.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
	}
	
}



















